#!../../bin/linux-x86_64/abs

## You may have to change abs to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/abs.dbd"
abs_registerRecordDeviceDriver pdbbase

epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "20000")
epicsEnvSet("ARRAY_SIZE", "2052")
#test
#epicsEnvSet("IOCDEV", "ccd1")

## Load record instances
#dbLoadRecords("db/xxx.db","user=epics")

cd "${TOP}/iocBoot/${IOC}"

dbLoadRecords("abs.db", "BL=LPQ,DEV=$(IOCDEV),SIZE=$(ARRAY_SIZE)")

iocInit

## Start any sequence programs
#seq sncxxx,"user=epics"
