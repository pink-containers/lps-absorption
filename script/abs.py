#!/usr/bin/python3

import numpy as np
import epics
import time
import threading
import os

print("** Absorption calc script **")
## catch IOCDEV
if "IOCDEV" in os.environ:
  DEV=os.environ['IOCDEV']
else:
  DEV="ccd1"
print(f"DEV is defined as: {DEV}")
## semaphore
#sem = threading.Semaphore()
eve = threading.Event()

## pv callback
def onchange(**kw):
  #sem.release()
  eve.set()

## connect PVs
refpv = epics.PV(f"LPQ:{DEV}:x:3:specX_RBV", auto_monitor=True, callback=onchange)
sampv = epics.PV(f"LPQ:{DEV}:x:5:specX_RBV", auto_monitor=True, callback=onchange)
datapv = epics.PV(f"LPQ:{DEV}:ABS:data", auto_monitor=False)
statuspv = epics.PV(f"LPQ:{DEV}:ABS:status", auto_monitor=False)
offsetpv = epics.PV(f"LPQ:{DEV}:ABS:offset_RBV", auto_monitor=False)
imgidpv = epics.PV(f"LPQ:{DEV}:cam1:ArrayCounter_RBV", auto_monitor=True)
calcidpv = epics.PV(f"LPQ:{DEV}:ABS:calc_id", auto_monitor=False)
#avgidpv = epics.PV(f"LPQ:{DEV}:ABS:avg_id", auto_monitor=False)

## PV connection delay
print("Connecting to PVs (3s)...")
time.sleep(3)

## main
print("[{}] Absorption calc script running...".format(time.asctime()))
calcid=0
while(True):
  try:
    #sem.acquire()
    eve.wait()
    time.sleep(0.05)
    # match array sizes
    ref = refpv.value
    Nr = len(ref)
    sam = sampv.value
    Ns = len(sam)
    if Nr<Ns:
      sam = sam[:Nr]
    elif Ns<Nr:
      ref = ref[:Ns]
    # add offset if lowest value <=0
    lmin = min(np.min(ref),np.min(sam))
    if lmin<=0:
      offset=1-lmin
      status=1
      ref = np.add(ref,offset)
      sam = np.add(sam,offset)
    else:
      offset=0
      status=0
    # create absorption data
    data = np.log(np.divide(ref,sam))
    datapv.put(data)
    # update RBV values
    statuspv.put(status)
    offsetpv.put(offset)
    imgid = imgidpv.get()
    if (imgid != calcid):
      calcid=imgid
      time.sleep(0.1)
      calcidpv.put(calcid)
    eve.clear()
  except Exception as err:
    print("[{}] Main loop error".format(time.asctime()))
    print(err)
    eve.clear()
    time.sleep(1)


print("OK")
